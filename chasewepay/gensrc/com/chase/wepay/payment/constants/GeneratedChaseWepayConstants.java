/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Sep 18, 2018 6:53:22 PM                     ---
 * ----------------------------------------------------------------
 */
package com.chase.wepay.payment.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedChaseWepayConstants
{
	public static final String EXTENSIONNAME = "chasewepay";
	
	protected GeneratedChaseWepayConstants()
	{
		// private constructor
	}
	
	
}
