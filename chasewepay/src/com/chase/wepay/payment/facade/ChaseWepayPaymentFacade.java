/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.chase.wepay.payment.facade;

/**
 * The Interface ChaseWepayPaymentFacade.
 *
 * @author skotni
 */
public interface ChaseWepayPaymentFacade
{
	/**
	 * Process credit card IPN. Deletes the credit card if the card state is 'deleted' and updates the credit card
	 * details if the card state is 'authorized'.
	 *
	 * @param creditCardId
	 *           - Long
	 */
	void processCreditCardIPN(Long creditCardId);

	/**
	 * Process checkout ID Sends an email to business if the capture fails
	 * 
	 * @param creditCardId
	 *           - Long
	 */
	void processCheckoutIPN(Long checkoutId);
}
