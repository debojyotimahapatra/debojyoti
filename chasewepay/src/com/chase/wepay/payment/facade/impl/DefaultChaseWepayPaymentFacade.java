/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.chase.wepay.payment.facade.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.model.process.ErrorNotificationMailProcessModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.processengine.model.ProcessTaskLogModel;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.ModelNotFoundException;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.chase.wepay.payment.constants.ChaseWepayConstants;
import com.chase.wepay.payment.facade.ChaseWepayPaymentFacade;
import com.chase.wepay.payment.helper.ChaseWepayHelper;
import com.chase.wepay.payment.service.ChaseWepayService;
import com.wepay.model.Checkout;


/**
 * The Class DefaultChaseWepayPaymentFacade.
 *
 * @author skotni
 */
public class DefaultChaseWepayPaymentFacade implements ChaseWepayPaymentFacade
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultChaseWepayPaymentFacade.class);

	/** The customer account service. */
	private CustomerAccountService customerAccountService;

	/** The chase wepay service. */
	private ChaseWepayService chaseWepayService;

	/** The chase wepay helper. */
	private ChaseWepayHelper chaseWepayHelper;

	/** The model service. */
	private ModelService modelService;

	/** The event service **/
	private EventService eventService;

	/** The business process service **/
	private BusinessProcessService businessProcessService;



	/**
	 * Process credit card IPN. Deletes the credit card if the card state is 'deleted' and updates the credit card
	 * details if the card state is 'authorized'.
	 *
	 * @param creditCardId
	 *           - Long
	 */
	@Override
	public void processCreditCardIPN(final Long creditCardId)
	{
		validateParameterNotNull(creditCardId, "CreditCardId cannot be null");
		final CreditCardPaymentInfoModel creditCardPaymentInfo = chaseWepayHelper
				.getCreditCardForSubscriptionId(creditCardId.toString());

		if (creditCardPaymentInfo == null)
		{
			throw new ModelNotFoundException("Credit card for id # " + creditCardId + " is not found");
		}

		// Get the card details from WePay along with card state
		final CardInfo cardInfo = chaseWepayService.getCreditCardDetails(creditCardId.toString(), false);

		final String cardState = cardInfo.getCardState();
		final String paymentDetailsId = creditCardPaymentInfo.getPk().toString();
		LOG.info("Card state returned from WePay is : " + cardState + " for # " + paymentDetailsId);

		if (ChaseWepayConstants.WEPAY_CREDITCARD_DELETED.equals(cardInfo.getCardState()))
		{
			// Delete the card if the status returned from WePay is 'deleted'
			LOG.info("Deleting credit card # " + paymentDetailsId + " while processing IPN");
			removeCCPaymentInfo(creditCardPaymentInfo);
		}
		else if (ChaseWepayConstants.WEPAY_CREDITCARD_AUTHORIZED.equals(cardInfo.getCardState()))
		{
			// Update the card details in Hybris if status is authroized
			LOG.info("Updating credit card # " + paymentDetailsId + " while processing IPN");
			updateCreditCardDetails(cardInfo, creditCardPaymentInfo);
		}
		else
		{
			LOG.error("Unsupported card state : " + cardState + " to process IPN for # " + creditCardPaymentInfo.getPk());
		}
	}

	/**
	 * Removes the CC payment info and updates the default payment info in Customer's profile
	 *
	 * @param creditCardPaymentInfo
	 *           - CreditCardPaymentInfoModel
	 */
	private void removeCCPaymentInfo(final CreditCardPaymentInfoModel creditCardPaymentInfo)
	{
		final UserModel userModel = creditCardPaymentInfo.getUser();
		validateParameterNotNull(userModel, "CreditCard's user cannot be null");

		// Update the default payment info in customer's profile
		final CustomerModel customerModel = (CustomerModel) userModel;

		// Deleting the credit card
		modelService.remove(creditCardPaymentInfo);
		modelService.refresh(customerModel);

		// Update the default payment info in user's profile
		if (customerModel.getDefaultPaymentInfo() == null)
		{
			final List<CreditCardPaymentInfoModel> ccPaymentInfos = customerAccountService.getCreditCardPaymentInfos(customerModel,
					true);
			if (CollectionUtils.isNotEmpty(ccPaymentInfos))
			{
				LOG.info("Updating the default payment info in User's profile for # " + creditCardPaymentInfo.getPk());
				customerAccountService.setDefaultPaymentInfo(customerModel, ccPaymentInfos.get(ccPaymentInfos.size() - 1));
			}
		}
	}

	/**
	 * Update credit card details.
	 *
	 * @param cardInfo
	 *           - CardInfo
	 * @param cardPaymentInfoModel
	 *           - CreditCardPaymentInfoModel
	 */
	private void updateCreditCardDetails(final CardInfo cardInfo, final CreditCardPaymentInfoModel cardPaymentInfoModel)
	{
		final String accountHolderName = cardInfo.getCardHolderFullName();
		if (StringUtils.isNotBlank(accountHolderName)
				&& !StringUtils.equalsIgnoreCase(accountHolderName, cardPaymentInfoModel.getCcOwner()))
		{
			cardPaymentInfoModel.setCcOwner(accountHolderName);
		}

		final String cardNumberMasked = cardInfo.getCardNumber();
		final String lastFourDigitsNew = getLastFourDigits(cardNumberMasked);

		final String existingCardNumber = cardPaymentInfoModel.getNumber();
		final String lastFourDigitsExisting = getLastFourDigits(existingCardNumber);
		if (StringUtils.isNotBlank(lastFourDigitsNew) && StringUtils.isNotBlank(lastFourDigitsExisting)
				&& !StringUtils.equals(lastFourDigitsNew, lastFourDigitsExisting))
		{
			final String cardPrefix = existingCardNumber.substring(0, (existingCardNumber.length() - 4));
			cardPaymentInfoModel.setNumber(cardPrefix + lastFourDigitsNew);
		}
		else
		{
			LOG.info("Card number is not modified for CCPaymentInfo # " + cardPaymentInfoModel.getPk() + " during CreditCard IPN");
		}

		final Integer expiryMonth = cardInfo.getExpirationMonth();
		if (expiryMonth != null && StringUtils.isNotBlank(cardPaymentInfoModel.getValidToMonth())
				&& expiryMonth != Integer.valueOf(cardPaymentInfoModel.getValidToMonth()))
		{
			cardPaymentInfoModel.setValidToMonth(String.format("%02d", cardInfo.getExpirationMonth()));
		}

		final Integer expiryYear = cardInfo.getExpirationYear();
		if (expiryYear != null && StringUtils.isNotBlank(cardPaymentInfoModel.getValidToYear())
				&& expiryYear != Integer.valueOf(cardPaymentInfoModel.getValidToYear()))
		{
			cardPaymentInfoModel.setValidToYear(String.valueOf(cardInfo.getExpirationYear()));
		}
		modelService.save(cardPaymentInfoModel);
	}

	protected String getLastFourDigits(final String cardNumber)
	{
		if (cardNumber != null && cardNumber.trim().length() > 4)
		{
			return cardNumber.trim().substring(cardNumber.length() - 4);
		}
		return null;
	}

	/**
	 * Update the status of the transaction based on the IPN
	 *
	 * @param checkoutId
	 *           - Long
	 *
	 */
	@Override
	public void processCheckoutIPN(final Long checkoutId)
	{
		validateParameterNotNull(checkoutId, "checkoutId cannot be null");
		final OrderModel orderModel = chaseWepayHelper.getOrderForCheckoutId(checkoutId);
		if (orderModel == null)
		{
			throw new ModelNotFoundException("CheckoutId doesnot have corresponding order");
		}

		final PointOfServiceModel mainDealer = orderModel.getMainDealer();
		validateParameterNotNull(mainDealer, "Dealer details for WePay - Payment capture are not found");
		final String guid = orderModel.getGuid();
		LOG.info("Entered method - fetchCHeckoutById for # " + guid);
		final String wepayAccessToken = mainDealer.getWepayAccessToken();
		final PaymentTransactionEntryModel paymentTransactionEntryModel;
		String CaptureTransactionStatus;
		String CaptureTransactionStatusDetails;

		final Checkout checkout = chaseWepayService.fetchCheckoutFromWepay(checkoutId, wepayAccessToken);
		final String state = checkout.getState();

		paymentTransactionEntryModel = getCapturePaymentTransaction(orderModel);
		validateParameterNotNull(paymentTransactionEntryModel, "There is no capture transaction on the order");

		if (ChaseWepayConstants.WEPAY_PAYMENT_CANCELLED.equalsIgnoreCase(state)
				|| ChaseWepayConstants.WEPAY_PAYMENT_FAILED.equalsIgnoreCase(state))
		{
			final List<String> errorMessages = new ArrayList();
			final String errorMsg = "Payment for Order# " + orderModel.getCode() + "& current status of the payment is " + state;
			errorMessages.add(errorMsg);
			//invoke  error notification email
			final ErrorNotificationMailProcessModel errorNotificationMailProcessModel = (ErrorNotificationMailProcessModel) businessProcessService
					.createProcess("errorNotification-" + orderModel.getCode() + "-" + System.currentTimeMillis(),
							"sendErrorNotificationMailProcess");

			final ProcessTaskLogModel taskLog = new ProcessTaskLogModel();
			taskLog.setActionId(ChaseWepayConstants.WEPAY_PAYMENT_FAILED_ACTION);
			taskLog.setLogMessages(errorMsg);

			errorNotificationMailProcessModel.setOrder(orderModel);
			errorNotificationMailProcessModel.setFailedAction(taskLog.getActionId());
			errorNotificationMailProcessModel.setErrorMesage(errorMessages);

			if (ChaseWepayConstants.WEPAY_PAYMENT_FAILED.equalsIgnoreCase(state))
			{
				CaptureTransactionStatus = TransactionStatus.ERROR.toString();
				CaptureTransactionStatusDetails = TransactionStatusDetails.CHASEWEPAY_ERROR.toString();
			}
			else
			{
				CaptureTransactionStatus = TransactionStatus.REJECTED.toString();
				CaptureTransactionStatusDetails = TransactionStatusDetails.PROCESSOR_DECLINE.toString();
			}
			modelService.save(errorNotificationMailProcessModel);
			businessProcessService.startProcess(errorNotificationMailProcessModel);
		}
		else if (ChaseWepayConstants.WEPAY_PAYMENT_CAPTURED.equalsIgnoreCase(state)
				|| ChaseWepayConstants.WEPAY_PAYMENT_RELEASED.equalsIgnoreCase(state))
		{
			CaptureTransactionStatus = TransactionStatus.ACCEPTED.toString();
			CaptureTransactionStatusDetails = TransactionStatusDetails.SUCCESFULL.toString();
		}
		else if (ChaseWepayConstants.WEPAY_PAYMENT_CHARGEDBACK.equalsIgnoreCase(state))
		{
			CaptureTransactionStatus = TransactionStatus.REJECTED.toString();
			CaptureTransactionStatusDetails = TransactionStatusDetails.CHARGED_BACK.toString();
		}
		else
		{
			CaptureTransactionStatus = TransactionStatus.ERROR.toString();
			CaptureTransactionStatusDetails = TransactionStatusDetails.UNKNOWN_CODE.toString();
			LOG.info("unknown checkout state");
		}
		paymentTransactionEntryModel.setTransactionStatus(CaptureTransactionStatus);
		paymentTransactionEntryModel.setTransactionStatusDetails(CaptureTransactionStatusDetails);
		modelService.save(paymentTransactionEntryModel);
	}



	/**
	 * Fetches the capture payment transaction in the order
	 *
	 * @params orderModel -OrderModel
	 * @returnPaymentTransactionEntryModel
	 */
	private PaymentTransactionEntryModel getCapturePaymentTransaction(final OrderModel orderModel)
	{
		final List<PaymentTransactionModel> paymentTransatctionModelList = orderModel.getPaymentTransactions();
		for (final PaymentTransactionModel paymentTransactionModel : paymentTransatctionModelList)
		{
			final List<PaymentTransactionEntryModel> paymentTransactionEntryModelList = paymentTransactionModel.getEntries();
			for (final PaymentTransactionEntryModel paymentTransactionEntryModel : paymentTransactionEntryModelList)
			{
				final String transactionStatus = paymentTransactionEntryModel.getTransactionStatus();
				final String transactionStatusDetails = paymentTransactionEntryModel.getTransactionStatusDetails();
				final PaymentTransactionType paymentTransactionType = paymentTransactionEntryModel.getType();

				if (PaymentTransactionType.CAPTURE.equals(paymentTransactionType)
						&& TransactionStatus.REVIEW.toString().equalsIgnoreCase(transactionStatus))
				{
					return paymentTransactionEntryModel;
				}
			}
		}
		return null;

	}

	/**
	 * @param chaseWepayService
	 *           - ChaseWepayService
	 */
	@Required
	public void setChaseWepayService(final ChaseWepayService chaseWepayService)
	{
		this.chaseWepayService = chaseWepayService;
	}

	/**
	 * @param chaseWepayHelper
	 *           - ChaseWepayHelper
	 */
	@Required
	public void setChaseWepayHelper(final ChaseWepayHelper chaseWepayHelper)
	{
		this.chaseWepayHelper = chaseWepayHelper;
	}

	/**
	 * @param modelService
	 *           - ModelService
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @param customerAccountService
	 *           the customerAccountService to set
	 */
	@Required
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}

	/**
	 * @return the eventService
	 */
	protected EventService getEventService()
	{
		return eventService;
	}


	/**
	 * @param businessProcessService
	 *           the businessProcessService to set
	 */
	@Required
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}

	/**
	 * @param eventService
	 *           the eventService to set
	 */
	@Required
	public void setEventService(final EventService eventService)
	{
		this.eventService = eventService;
	}

}
