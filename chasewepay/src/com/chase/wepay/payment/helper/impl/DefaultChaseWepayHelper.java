/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.chase.wepay.payment.helper.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.util.DiscountValue;
import de.hybris.platform.util.TaxValue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.chase.wepay.payment.dao.ChaseWepayDao;
import com.chase.wepay.payment.helper.ChaseWepayHelper;
import com.wepay.payment.ItemReceiptData;


/**
 * The Class DefaultChaseWepayHelper.
 *
 */
public class DefaultChaseWepayHelper implements ChaseWepayHelper
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(DefaultChaseWepayHelper.class);

	/** The chase wepay dao. */
	private ChaseWepayDao chaseWepayDao;

	/**
	 * Returns the transaction item receipt details.
	 *
	 * @param abstractOrder
	 *           - AbstractOrderModel
	 * @return List<ItemReceiptData>
	 */
	@Override
	public List<ItemReceiptData> getTransactionItemReceiptDetails(final AbstractOrderModel abstractOrder)
	{
		LOG.info("Preparing item receipt details for # " + abstractOrder.getGuid());
		final List<ItemReceiptData> details = new ArrayList<>();
		final String currencyCode = abstractOrder.getCurrency().getIsocode();
		final List<AbstractOrderEntryModel> entries = abstractOrder.getEntries();
		if (CollectionUtils.isNotEmpty(entries))
		{
			for (final AbstractOrderEntryModel entry : entries)
			{
				details.addAll(getEntryReceiptDetails(entry, currencyCode));
			}
		}

		// Order level tax
		final double totalTax = calculateTotalTax(abstractOrder.getTotalTaxValues());
		if (totalTax != 0)
		{
			final String taxDescription = "Order_Tax";
			details.add(buildReceipt(taxDescription, 1, totalTax, totalTax, currencyCode));
		}

		// Order level delivery cost
		final double deliveryCost = abstractOrder.getDeliveryCost().doubleValue();
		if (deliveryCost != 0)
		{
			final String deliveryCostDesc = "Order_DeliveryCost";
			details.add(buildReceipt(deliveryCostDesc, 1, deliveryCost, deliveryCost, currencyCode));
		}

		// Order level discount
		final double totalDiscount = calculateTotalDiscount(abstractOrder.getGlobalDiscountValues());
		if (totalDiscount != 0)
		{
			final String discountDescription = "Order_Discount";
			details.add(buildReceipt(discountDescription, 1, totalDiscount, totalDiscount, currencyCode));
		}

		return details;
	}

	/**
	 * Returns the order entry receipt details.
	 *
	 * @param orderEntry
	 *           - AbstractOrderEntryModel
	 * @param currencyCode
	 *           - String
	 * @return List<ItemReceiptData>
	 */
	private List<ItemReceiptData> getEntryReceiptDetails(final AbstractOrderEntryModel orderEntry, final String currencyCode)
	{
		final List<ItemReceiptData> entryReceiptDetails = new ArrayList<>();

		// Actual product details
		final ProductModel productModel = orderEntry.getProduct();
		final Integer entryNumber = orderEntry.getEntryNumber();
		final String description = "Item_Name_" + entryNumber + " " + productModel.getName();
		final long quantity = orderEntry.getQuantity().longValue();
		final double basePrice = orderEntry.getBasePrice().doubleValue();
		final double entryTotal = quantity * basePrice;
		entryReceiptDetails.add(buildReceipt(description, quantity, basePrice, entryTotal, currencyCode));

		// Entry Taxes
		final double totalEntryTax = calculateTotalTax(orderEntry.getTaxValues());
		if (totalEntryTax != 0)
		{
			final String taxDescription = "Item_Tax_" + entryNumber;
			entryReceiptDetails.add(buildReceipt(taxDescription, 1, totalEntryTax, totalEntryTax, currencyCode));
		}

		// Entry Discounts
		final double totalEntryDiscount = calculateTotalDiscount(orderEntry.getDiscountValues());
		if (totalEntryDiscount != 0)
		{
			final String discountDescription = "Item_Discount_" + entryNumber;
			entryReceiptDetails.add(buildReceipt(discountDescription, 1, totalEntryDiscount, totalEntryDiscount, currencyCode));
		}

		return entryReceiptDetails;
	}

	/**
	 * Calculate total tax.
	 *
	 * @param entryTaxes
	 *           - Collection<TaxValue>
	 * @return double
	 */
	private double calculateTotalTax(final Collection<TaxValue> entryTaxes)
	{
		BigDecimal totalTax = BigDecimal.ZERO;
		if (CollectionUtils.isNotEmpty(entryTaxes))
		{
			for (final TaxValue taxValue : entryTaxes)
			{
				totalTax = totalTax.add(BigDecimal.valueOf(taxValue.getAppliedValue()));
			}
		}
		return totalTax.setScale(2, RoundingMode.HALF_EVEN).doubleValue();
	}

	/**
	 * Calculate total discount.
	 *
	 * @param entryDiscounts
	 *           - Collection<DiscountValue>
	 * @return double
	 */
	private double calculateTotalDiscount(final Collection<DiscountValue> entryDiscounts)
	{
		BigDecimal totalDiscount = BigDecimal.ZERO;
		if (CollectionUtils.isNotEmpty(entryDiscounts))
		{
			for (final DiscountValue discountValue : entryDiscounts)
			{
				totalDiscount = totalDiscount.add(BigDecimal.valueOf(discountValue.getAppliedValue()));
			}
		}
		return totalDiscount.negate().setScale(2, RoundingMode.HALF_EVEN).doubleValue();
	}

	/**
	 * Builds the receipt.
	 *
	 * @param description
	 *           - String
	 * @param quantity
	 *           - long
	 * @param itemPrice
	 *           - double
	 * @param amount
	 *           - double
	 * @param currency
	 *           - String
	 * @return ItemReceiptData
	 */
	private ItemReceiptData buildReceipt(final String description, final long quantity, final double itemPrice,
			final double amount, final String currency)
	{
		final ItemReceiptData itemReceipt = new ItemReceiptData();
		itemReceipt.setDescription(description);
		itemReceipt.setQuantity(Long.valueOf(quantity));
		itemReceipt.setItem_price(Double.valueOf(itemPrice));
		itemReceipt.setAmount(Double.valueOf(amount));
		return itemReceipt;
	}

	/**
	 * Returns the credit card for subscription id.
	 *
	 * @param subscriptionId
	 *           - String
	 * @return CreditCardPaymentInfoModel
	 */
	@Override
	public CreditCardPaymentInfoModel getCreditCardForSubscriptionId(final String subscriptionId)
	{
		return chaseWepayDao.findCreditCardForSubscriptionId(subscriptionId);
	}

	/**
	 * Returns the orders that are pending for capture payment
	 *
	 * @param subscriptionId
	 *           - String
	 * @return List<OrderModel>
	 */
	@Override
	public List<OrderModel> getPendingOrdersForCapture(final String subscriptionId)
	{
		return chaseWepayDao.getPendingOrdersForCapture(subscriptionId);
	}

	/*
	 * Find the order for checkoutId
	 *
	 * @param checkoutId - Long
	 *
	 * @return OrderModel
	 */
	@Override
	public OrderModel getOrderForCheckoutId(final Long checkoutId)
	{
		return chaseWepayDao.getOrderForCheckoutId(checkoutId);
	}

	/**
	 * @param chaseWepayDao
	 *           - ChaseWepayDao
	 */
	@Required
	public void setChaseWepayDao(final ChaseWepayDao chaseWepayDao)
	{
		this.chaseWepayDao = chaseWepayDao;
	}

}
