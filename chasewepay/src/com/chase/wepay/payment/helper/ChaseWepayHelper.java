/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.chase.wepay.payment.helper;

import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;

import java.util.List;

import com.wepay.payment.ItemReceiptData;


/**
 * ChaseWepayHelper
 *
 * @author skotni
 */
public interface ChaseWepayHelper
{

	/**
	 * Returns the transaction item receipt details.
	 *
	 * @param abstractOrder
	 *           - AbstractOrderModel
	 * @return List<ItemReceiptData>
	 */
	List<ItemReceiptData> getTransactionItemReceiptDetails(AbstractOrderModel abstractOrder);

	/**
	 * Returns the credit card for subscription id.
	 *
	 * @param subscriptionId
	 *           - String
	 * @return CreditCardPaymentInfoModel
	 */
	CreditCardPaymentInfoModel getCreditCardForSubscriptionId(String subscriptionId);

	/**
	 * Returns the orders that are pending for capture payment
	 *
	 * @param subscriptionId
	 *           - String
	 * @return List<OrderModel>
	 */
	List<OrderModel> getPendingOrdersForCapture(String subscriptionId);

	/*
	 * Find the order for checkoutId
	 *
	 * @param checkoutId - Long
	 * 
	 * @return OrderModel
	 */
	OrderModel getOrderForCheckoutId(Long checkoutId);

}
