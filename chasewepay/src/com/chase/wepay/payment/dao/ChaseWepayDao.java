/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.chase.wepay.payment.dao;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.servicelayer.internal.dao.Dao;

import java.util.List;


/**
 * The Interface ChaseWepayDao.
 *
 * @author skotni
 */
public interface ChaseWepayDao extends Dao
{
	/**
	 * Find the credit card for subscription id.
	 *
	 * @param subscriptionId
	 *           - String
	 * @return CreditCardPaymentInfoModel
	 */
	CreditCardPaymentInfoModel findCreditCardForSubscriptionId(String subscriptionId);

	/**
	 * Find the order for checkoutId
	 *
	 * @param checkoutId
	 *           - Long
	 * @return OrderModel
	 */
	OrderModel getOrderForCheckoutId(Long checkoutId);

	/**
	 * Returns the orders that are pending for capture payment
	 *
	 * @param subscriptionId
	 *           - String
	 * @return List<OrderModel>
	 */
	List<OrderModel> getPendingOrdersForCapture(String subscriptionId);

}
