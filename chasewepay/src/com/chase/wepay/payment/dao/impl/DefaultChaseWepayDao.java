/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.chase.wepay.payment.dao.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.enums.OrderStatus;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.jalo.order.Order;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.servicelayer.internal.dao.AbstractItemDao;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import com.chase.wepay.payment.dao.ChaseWepayDao;


/**
 * The Class DefaultChaseWepayDao.
 *
 * @author skotni
 */
public class DefaultChaseWepayDao extends AbstractItemDao implements ChaseWepayDao
{
	private static final Logger LOG = Logger.getLogger(DefaultChaseWepayDao.class);

	/** The Constant FIND_CCPAYMENT_INFO_BY_SUBSCRIPTION_QUERY. */
	private static final String FIND_CCPAYMENT_INFO_BY_SUBSCRIPTION_QUERY = "SELECT {" + CreditCardPaymentInfoModel.PK + "} FROM {"
			+ CreditCardPaymentInfoModel._TYPECODE + "} WHERE {" + CreditCardPaymentInfoModel.SUBSCRIPTIONID
			+ "} = ?subscriptionId AND {" + CreditCardPaymentInfoModel.DUPLICATE + "} = ?duplicate";

	/** The Constant FIND_ORDER_BY_CHECKOUT_ID **/
	private static final String FIND_ORDER_BY_CHECKOUT_ID = "select {o:" + Order.PK + "} from {"
			+ PaymentTransactionModel._TYPECODE + " as t join order as o on {o:" + Order.PK + "}" + "= {t:"
			+ PaymentTransactionModel.ORDER + "} join " + PaymentTransactionEntryModel._TYPECODE + " as pte on {t:"
			+ PaymentTransactionModel.PK + "} = {pte:" + PaymentTransactionEntryModel.PAYMENTTRANSACTION + "}} where {pte:"
			+ PaymentTransactionModel.REQUESTTOKEN + "} = ?checkoutId";


	/**
	 * Find the credit card for subscription id.
	 *
	 * @param subscriptionId
	 *           - String
	 * @return CreditCardPaymentInfoModel
	 */
	@Override
	public CreditCardPaymentInfoModel findCreditCardForSubscriptionId(final String subscriptionId)
	{
		validateParameterNotNull(subscriptionId, "SubscriptionId must not be null");
		final Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put(CreditCardPaymentInfoModel.SUBSCRIPTIONID, subscriptionId);
		queryParams.put(CreditCardPaymentInfoModel.DUPLICATE, Boolean.FALSE);
		final SearchResult<CreditCardPaymentInfoModel> result = getFlexibleSearchService()
				.search(FIND_CCPAYMENT_INFO_BY_SUBSCRIPTION_QUERY, queryParams);
		return result.getCount() > 0 ? result.getResult().get(0) : null;
	}

	/**
	 * Returns the orders that are pending for capture payment
	 *
	 * Rules for capture pending orders:
	 * 1. When an order is placed it will only have authorized transaction entries
	 * 2. When capture payment request is initiated then the order will have capture transaction entry with review status
	 * 3. Order status should not be cancelled.
	 * 
	 * @param subscriptionId
	 *           - String
	 * @return List<OrderModel>
	 */
	@Override
	public List<OrderModel> getPendingOrdersForCapture(final String subscriptionId)
	{
		LOG.info("Inside method - getPendingOrdersForCapture");
		final String query = "SELECT DISTINCT {ord:pk} FROM {order AS ord JOIN creditcardpaymentinfo AS cc ON {ord:paymentinfo}={cc:pk} "
				+ "JOIN paymenttransaction AS pt ON {ord:pk} = {pt:order}} WHERE {cc:subscriptionid} = ?subscriptionId AND {ord:status} != ?orderStatus "
				+ "AND (Exists ({{SELECT 1 from {paymenttransactionentry AS pte1} WHERE {pte1:paymenttransaction} = {pt:pk} AND "
				+ "{pte1:type} = ?captureTxnType AND {pte1:transactionStatus} = ?captureStatus }}) "
				+ "OR "
				+ "EXISTS ({{SELECT 1 from {paymenttransactionentry AS pte2} WHERE {pte2:type} = ?authTxnType AND {pte2:paymenttransaction}={pt:pk}"
				+ "AND {pte2:transactionStatus} = ?authStatus AND 1 = ({{SELECT COUNT(*) FROM {paymenttransactionentry as pte3} where {pte3:paymenttransaction} = {pt:pk}}} }})))";
		validateParameterNotNull(subscriptionId, "SubscriptionId must not be null");
		final Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("subscriptionId", subscriptionId);
		queryParams.put("orderStatus", OrderStatus.CANCELLED);
		queryParams.put("captureTxnType", PaymentTransactionType.CAPTURE);
		queryParams.put("captureStatus", TransactionStatus.REVIEW.toString());
		queryParams.put("authTxnType", PaymentTransactionType.AUTHORIZATION);
		queryParams.put("authStatus", TransactionStatus.ACCEPTED.toString());
		final SearchResult<OrderModel> result = getFlexibleSearchService().search(query,
				queryParams);
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Capture pending order query : " + query + " & params : " + queryParams);
		}
		return result.getResult();
	}

	/*
	 * Find the order for checkoutId
	 *
	 * @param checkoutId - Long
	 * 
	 * @return OrderModel
	 */
	@Override
	public OrderModel getOrderForCheckoutId(final Long checkoutId)
	{
		LOG.info("started execution in defaultChaseWepayDao");
		final FlexibleSearchQuery fquery = new FlexibleSearchQuery(FIND_ORDER_BY_CHECKOUT_ID);
		fquery.addQueryParameter("checkoutId", checkoutId);
		final SearchResult<OrderModel> searchResult = getFlexibleSearchService().search(fquery);
		final List<OrderModel> orderList = searchResult.getResult();
		if (CollectionUtils.isNotEmpty(orderList))
		{
			return orderList.get(0);
		}
		return null;
	}

}
