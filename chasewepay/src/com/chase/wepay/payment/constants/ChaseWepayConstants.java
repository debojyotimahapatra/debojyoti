/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.chase.wepay.payment.constants;

/**
 * Global class for all Chasewepay constants. You can add global constants for your extension into this class.
 */
public final class ChaseWepayConstants extends GeneratedChaseWepayConstants
{
	public static final String EXTENSIONNAME = "chasewepay";

	private ChaseWepayConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
	public static final String WEPAY_API_ENDPOINT = "chasewepay.restapi.url";
	public static final String WEPAY_API_CLIENT_ID = "chasewepay.restapi.clientid";
	public static final String WEPAY_API_CLIENT_SECRET = "chasewepay.restapi.clientsecret";
	public static final String WEPAY_CARDTYPE_MAPPING = "chasewepay.cardtype.mappings";
	public static final String WEPAY_ERRORCODE_PREFIX = "chasewepay.payment.errorcodes.";
	public static final String WEPAY_RBIT_TYPE_PROPERTIES_MAPPING = "chasewepay.rbit.type.properties.mapping";
	public static final String WEPAY_MERCHNT_REGISTRATION_RBIT_SOURCE = "chasewepay.merchant.registration.rbit.source";
	public static final String WEPAY_MERCHNT_REGISTRATION_RBIT_TYPES = "chasewepay.merchant.registration.rbit.types";
	public static final String WEPAY_MERCHNT_REGISTRATION_SCOPE = "chasewepay.merchant.registration.scope";
	public static final String WEPAY_MERCHNT_REGISTRATION_DEVICE = "chasewepay.merchant.registration.device";
	public static final String WEPAY_CONNECTIVITY_RETRY_MILLISECONDS = "chasewepay.connectivity.retry.milliseconds";

	public static final String WEPAY_PROPERTY_ORDER_PAYMENT_TYPE = "chasewepay.payment.order.type";
	public static final String WEPAY_PROPERTY_ORDER_PAYMENT_DESCRIPTION = "chasewepay.payment.order.description";
	public static final String WEPAY_PROPERTY_ORDER_PAYMENT_FEEPAYER = "chasewepay.payment.order.feepayer";
	public static final String WEPAY_PROPERTY_PAYMENT_METHODTYPE = "chasewepay.payment.methodtype.";
	public static final String WEPAY_PROPERTY_PAYMENT_CHECKOUTRBIT_SOURCE = "chasewepay.payment.checkoutrbit.source";
	public static final String WEPAY_PROPERTY_PAYMENT_CHECKOUTRBIT_TYPEMAPPING = "chasewepay.payment.checkoutrbit.typemapping";

	public static final String TRANSACTION_STATUS = "TransactionStatus";
	public static final String TRANSACTION_STATUS_DETAILS = "TransactionStatusDetails";
	public static final String WEPAY_AUTHORIZATION_CHECKOUT_ID = "AuthTransactionCheckoutId";
	public static final String WEPAY_AUTHORIZATION_UNIQUE_ID = "AuthTransactionUniqueId";

	public static final String WEPAY_CAPTURE_CHECKOUT_ID = "CaptureTransactionCheckoutId";


	public static final String WEPAY_PAYMENT_AUTHORIZED = "authorized";

	public static final String WEPAY_CREDITCARD_DELETED = "deleted";
	public static final String WEPAY_CREDITCARD_AUTHORIZED = "authorized";
	public static final String WEPAY_PAYMENT_CAPTURED = "captured";
	public static final String WEPAY_PAYMENT_CHARGEDBACK = "chargeback";
	public static final String WEPAY_PAYMENT_CANCELLED = "cancelled";
	public static final String WEPAY_PAYMENT_RELEASED = "released";
	public static final String WEPAY_PAYMENT_REFUNDED = "refunded";
	public static final String WEPAY_PAYMENT_FAILED = "failed";

	public static final String WEPAY_PAYMENT_FAILED_ACTION = "captureFailedThroughIPN";
	public static final String WEPAY_CHECKOUT_IPN_URL = "chasewepay.checkout.ipn.notification.url";


}
