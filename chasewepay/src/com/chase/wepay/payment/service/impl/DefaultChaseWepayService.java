/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.chase.wepay.payment.service.impl;
import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.core.enums.CreditCardType;
import de.hybris.platform.core.model.c2l.CountryModel;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.c2l.RegionModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.enumeration.EnumerationService;
import de.hybris.platform.mobileservices.model.text.PhoneNumberModel;
import de.hybris.platform.mobileservices.model.text.UserPhoneNumberModel;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import com.chase.wepay.payment.constants.ChaseWepayConstants;
import com.chase.wepay.payment.exception.PaymentException;
import com.chase.wepay.payment.helper.ChaseWepayHelper;
import com.chase.wepay.payment.service.ChaseWepayService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wepay.WePay;
import com.wepay.exception.WePayException;
import com.wepay.model.Account;
import com.wepay.model.Checkout;
import com.wepay.model.CreditCard;
import com.wepay.model.User;
import com.wepay.model.data.AccountData;
import com.wepay.model.data.CheckoutData;
import com.wepay.model.data.FeeData;
import com.wepay.model.data.HeaderData;
import com.wepay.model.data.PMCreditCardData;
import com.wepay.model.data.PaymentMethodData;
import com.wepay.model.data.RbitData;
import com.wepay.model.data.UserData;
import com.wepay.net.WePayResource;
import com.wepay.payment.AddressData;
import com.wepay.payment.ItemReceiptData;


/**
 * DefaultChaseWepayService This class is used for the integration of ChaseWepay for different payment operations.
 *
 * @author skotni
 */
public class DefaultChaseWepayService implements ChaseWepayService
{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(DefaultChaseWepayService.class);

	/** The Constant SEPARATOR_SPACE. */
	private static final String SEPARATOR_SPACE = " ";

	/** The model service. */
	private ModelService modelService;

	/** The flexible search service. */
	private FlexibleSearchService flexibleSearchService;

	/** The configuration service. */
	private ConfigurationService configurationService;

	/** The enumeration service. */
	private EnumerationService enumerationService;

	/** The card type mapping. */
	private Map<String, String> cardTypeMapping;

	/** Rbit type - properties mapping */
	private Map<String, List<String>> rbitTypePropertiesMapping;

	/** The checkout rbit mapping. */
	private Map<String, String> checkoutRbitMap;

	/** The chase wepay helper. */
	private ChaseWepayHelper chaseWepayHelper;

	/** objectMapper to print request object */
	final ObjectMapper objectMapper = new ObjectMapper();

	/**
	 * This method will be called during spring initialization of the bean 'chaseWepayService'. It will initialize the
	 * WePay resources present in the chasewepay-java-sdk jar with clientid, clientsecret and apiendpoint. It will also
	 * initialize the credit card type mapping of ChaseWepay vs Hybris.
	 */
	public void initializeWepay()
	{
		LOG.info("Initializing the ChaseWepay SDK with client and api endpoint details");
		final String apiEndPoint = getProperty(ChaseWepayConstants.WEPAY_API_ENDPOINT);
		final Long apiClientId = Long.parseLong(getProperty(ChaseWepayConstants.WEPAY_API_CLIENT_ID));
		final String apiClientSecret = getProperty(ChaseWepayConstants.WEPAY_API_CLIENT_SECRET);
		WePay.initialize(apiClientId, apiClientSecret, apiEndPoint, null);
		LOG.info("WePay API details are setup with endpoint : " + WePayResource.apiEndpoint);

		cardTypeMapping = new HashMap<>();
		final String cardTypeMap = getProperty(ChaseWepayConstants.WEPAY_CARDTYPE_MAPPING);
		if (StringUtils.isNotBlank(cardTypeMap))
		{
			final String[] cardTypeArray = cardTypeMap.split(",");
			final List<String> cardTypeList = Arrays.asList(cardTypeArray);
			for (final String mapping : cardTypeList)
			{
				final String[] keyValue = mapping.split(":");
				if (keyValue.length == 2)
				{
					cardTypeMapping.put(keyValue[0], keyValue[1]);
				}
			}
		}
		LOG.info("CreditCard type mapping size : " + cardTypeMapping.size());

		initializeRbitTypePropertiesMapping();
	}

	/**
	 * Initialize rbit type properties mapping.
	 */
	private void initializeRbitTypePropertiesMapping()
	{
		rbitTypePropertiesMapping = new HashMap();
		final String rbitTypeProperties = configurationService.getConfiguration()
				.getString(ChaseWepayConstants.WEPAY_RBIT_TYPE_PROPERTIES_MAPPING);
		if (StringUtils.isNotBlank(rbitTypeProperties))
		{
			final String[] keyValueStrings = rbitTypeProperties.split(Pattern.quote("|"));
			for (final String keyValueString : keyValueStrings)
			{
				final String[] keyValue = keyValueString.split(":");
				if (keyValue.length == 2)
				{
					final String[] valueListArray = keyValue[1].split(",");
					final List<String> valueList = Arrays.asList(valueListArray);
					rbitTypePropertiesMapping.put(keyValue[0], valueList);
				}
			}
		}

		// Checkout rbit mapping
		checkoutRbitMap = new HashMap<>();
		final String checkoutRbitTypeMapping = getProperty(ChaseWepayConstants.WEPAY_PROPERTY_PAYMENT_CHECKOUTRBIT_TYPEMAPPING);
		LOG.info("Configured checkout rbit mapping : " + checkoutRbitTypeMapping);
		if (StringUtils.isNotBlank(checkoutRbitTypeMapping))
		{
			final String[] checkoutRbitMapArr = checkoutRbitTypeMapping.split(Pattern.quote("|"));
			final List<String> checkoutRbitList = Arrays.asList(checkoutRbitMapArr);
			LOG.info("Configured checkout rbitList : " + checkoutRbitList);

			checkoutRbitList.stream().forEach(checkoutRbitStr -> {
				final String[] rbitArray = checkoutRbitStr.split(":");
				if (rbitArray.length == 2)
				{
					final String name = rbitArray[0];
					final String typeStr = rbitArray[1];
					checkoutRbitMap.put(name, typeStr);
				}
			});
			LOG.info("Checkout rbits configured are : " + checkoutRbitMap);
		}
	}

	/**
	 * This method will call wepay API /credit_card/authorize once the credit_card_id is received from FrontEnd i.e.
	 * isNewCreditCard is true.
	 *
	 * It will fetch credit card details using wepay API /credit_card i.e. isNewCreditCard is false.
	 *
	 * Response of this API contains all the credit card details.
	 *
	 * @param creditCardId
	 *           - String
	 * @param isNewCreditCard
	 *           - boolean
	 * @return CardInfo
	 */
	@Override
	public CardInfo getCreditCardDetails(final String creditCardId, final boolean isNewCreditCard)
	{
		validateParameterNotNull(creditCardId, "CreditCardId cannot be null");
		if (!StringUtils.isNumeric(creditCardId))
		{
			throw new PaymentException("CreditCardId should be numeric");
		}
		try
		{
			CreditCard creditCard = null;

			if (isNewCreditCard)
			{
				// If it is a new credit card then it needs to be authorized in WePay
				// WePay API /credit_card/authorize is used to authorize and get the card details
				creditCard = CreditCard.authorizeCard(Long.valueOf(creditCardId));
			}
			else
			{
				// If it is an existing card then it can be directly fetched from WePay
				// WePay API /credit_card is used to get the card details
				creditCard = CreditCard.fetch(Long.valueOf(creditCardId), new HeaderData()); // No API headers are required
			}

			validateParameterNotNull(creditCard, "CreditCard result cannot be null");
			final CardInfo cardInfo = new CardInfo();
			cardInfo.setCardHolderFullName(creditCard.getUserName());
			cardInfo.setCardState(creditCard.getState());

			final String creditCardName = creditCard.getCreditCardName(); // E.g. value will be in the format of : Visa xxxxxx4018
			if (StringUtils.isNotBlank(creditCardName))
			{
				final String trimmedName = StringUtils.trimToNull(creditCardName);
				final String[] nameArray = new String[]
				{ StringUtils.substringBeforeLast(trimmedName, SEPARATOR_SPACE),
						StringUtils.substringAfterLast(trimmedName, SEPARATOR_SPACE) };

				if (nameArray.length == 2)
				{
					final String cardType = nameArray[0];
					final String maskedCardNumber = nameArray[1];
					cardInfo.setCardNumber(maskedCardNumber);
					final String cardTypeValue = cardTypeMapping.get(cardType);
					final CreditCardType creditCardType = (CreditCardType) enumerationService
							.getEnumerationValue(CreditCardType.class.getSimpleName(), cardTypeValue);
					cardInfo.setCardType(creditCardType);
				}
				else
				{
					LOG.error("Credit card name from WePay API is invalid " + trimmedName + " for credit_card_id : " + creditCardId);
					throw new PaymentException("Invalid Credit card name");
				}
			}
			cardInfo.setExpirationMonth(creditCard.getExpirationMonth());
			cardInfo.setExpirationYear(creditCard.getExpirationYear());
			return cardInfo;
		}
		catch (final JSONException | IOException e)
		{
			LOG.error("IOException|JSONException occurred while authorizing credit card #" + creditCardId, e);
			throw new PaymentException(TransactionStatusDetails.COMMUNICATION_PROBLEM.toString(), e.getCause());
		}
		catch (final WePayException e)
		{
			LOG.error("WePayException occurred while authorizing credit card #" + creditCardId + getErrorMessage(e), e);
			throw new PaymentException(processErrorDetails(e).toString(), e.getCause());
		}
	}

	/**
	 * This method will call Wepay API /credit_card/delete to delete a credit card added by customer.
	 *
	 * @param creditCardId
	 *           - Long
	 * @return boolean
	 */
	@Override
	public boolean deleteCreditCard(final Long creditCardId)
	{
		// Initialize wepay CreditCard with the creditCardId
		final CreditCard creditCard = new CreditCard(creditCardId);
		try
		{
			// Call the delete method to delete it in WePay
			creditCard.delete(new HeaderData());
			LOG.info("CreditCard with id # " + creditCardId + " has been deleted at WePay");
			return true;
		}
		catch (JSONException | IOException e)
		{
			LOG.error("IOException|JSONException occurred while deleting credit card #" + creditCardId, e);
		}
		catch (final WePayException e)
		{
			LOG.error("WePayException occurred while deleting the credit card #" + creditCardId + getErrorMessage(e), e);
		}
		return false;
	}

	/**
	 * This method will call Wepay API /checkout/create to do final payment authorization.
	 *
	 * @param abstractOrder
	 *           - AbstractOrderModel
	 * @param totalAmount
	 *           the - BigDecimal
	 * @param currencyCode
	 *           - String
	 * @return {@link PaymentData}
	 */
	@Override
	public PaymentData authorizePayment(final AbstractOrderModel abstractOrder, final BigDecimal totalAmount,
			final String currencyCode, final boolean autoCapture)
	{
		final PointOfServiceModel mainDealer = abstractOrder.getMainDealer();
		validateParameterNotNull(mainDealer, "Dealer details for WePay - Payment authorization are not found");
		final String guid = abstractOrder.getGuid();
		LOG.info("Entered method - authorizePayment for # " + guid);
		final Long wepayAccountId = mainDealer.getWepayAccountId();
		validateParameterNotNull(wepayAccountId, "Dealer's WePay account number cannot be null");
		final String wepayAccessToken = mainDealer.getWepayAccessToken();
		validateParameterNotNull(wepayAccessToken, "Dealer's WePay access token cannot be null");

		// Prepare the authorization request
		final CheckoutData checkoutRequest = preparePaymentAuthorizationRequest(abstractOrder, totalAmount, currencyCode,
				wepayAccountId, autoCapture);

		Checkout checkoutResponse = null;
		try
		{
			// Checkout.create() will call /checkout/create API of wepay to perform final payment authorization
			checkoutResponse = Checkout.create(checkoutRequest, wepayAccessToken);
			final Long checkoutId = checkoutResponse.getCheckoutId();

			final PaymentData authResult = new PaymentData();
			final Map<String, String> authResponseParams = new HashMap<>();
			authResult.setParameters(authResponseParams);
			if (ChaseWepayConstants.WEPAY_PAYMENT_AUTHORIZED.equals(checkoutResponse.getState()) && checkoutId != null)
			{
				authResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS, TransactionStatus.ACCEPTED.toString());
				authResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS_DETAILS,
						TransactionStatusDetails.SUCCESFULL.toString());
				authResponseParams.put(ChaseWepayConstants.WEPAY_AUTHORIZATION_CHECKOUT_ID, checkoutId.toString());
				authResponseParams.put(ChaseWepayConstants.WEPAY_AUTHORIZATION_UNIQUE_ID, checkoutRequest.uniqueId);
			}
			else
			{
				LOG.error("Payment authroization failed for # " + guid + " state : " + checkoutResponse.getState()
						+ " & checkoutId : " + checkoutId);
				authResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS, TransactionStatus.ERROR.toString());
				authResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS_DETAILS,
						TransactionStatusDetails.AUTHORIZATION_REJECTED_BY_PSP.toString());
			}

			if (LOG.isDebugEnabled())
			{
				LOG.debug("Authorization request : " + WePayResource.gson.toJson(checkoutRequest) + " && Authorization response : "
						+ WePayResource.gson.toJson(checkoutResponse));
			}
			LOG.info("Exit method - authorizePayment for # " + guid);
			return authResult;
		}
		catch (JSONException | IOException e)
		{
			LOG.error("IOException|JSONException occurred while authorizing payment for cart # " + guid, e);
			throw new PaymentException(TransactionStatusDetails.COMMUNICATION_PROBLEM.toString(), e.getCause());
		}
		catch (final WePayException e)
		{
			LOG.error("WePayException occurred while authorizing payment for cart # " + guid + " & Error : " + getErrorMessage(e),
					e);
			throw new PaymentException(processErrorDetails(e).toString(), e.getCause());
		}
	}

	/**
	 * Prepare payment authorization request.
	 *
	 * @param abstractOrder
	 *           - AbstractOrderModel
	 * @param totalAmount
	 *           - BigDecimal
	 * @param currencyCode
	 *           - String
	 * @param wepayAccountId
	 *           - Long
	 * @param autoCapture
	 *           - boolean
	 * @return CheckoutData
	 */
	private CheckoutData preparePaymentAuthorizationRequest(final AbstractOrderModel abstractOrder, final BigDecimal totalAmount,
			final String currencyCode, final Long wepayAccountId, final boolean autoCapture)
	{
		LOG.info("Preparing payment authorization request for # " + abstractOrder.getGuid());
		final String guid = abstractOrder.getGuid();
		final CheckoutData checkoutRequest = new CheckoutData();
		final String callBackURI = configurationService.getConfiguration().getString(ChaseWepayConstants.WEPAY_CHECKOUT_IPN_URL);
		validateParameterNotNull(callBackURI, "Wepay Callback URI for checkout cannot be null");

		// Set the basic details
		checkoutRequest.amount = totalAmount.setScale(2, RoundingMode.HALF_EVEN); // Cart total
		checkoutRequest.accountId = wepayAccountId; // Dealer's wepay account id
		checkoutRequest.uniqueId = abstractOrder.getCode() + Calendar.getInstance().getTimeInMillis(); // UniqueId
		checkoutRequest.currency = currencyCode; // Currency for the authorized amount
		checkoutRequest.type = getProperty(ChaseWepayConstants.WEPAY_PROPERTY_ORDER_PAYMENT_TYPE);
		checkoutRequest.shortDescription = getProperty(ChaseWepayConstants.WEPAY_PROPERTY_ORDER_PAYMENT_DESCRIPTION);

		// Set the payment method details
		final PaymentMethodData paymentMethod = new PaymentMethodData();
		if (abstractOrder.getPaymentInfo() instanceof CreditCardPaymentInfoModel)
		{
			final CreditCardPaymentInfoModel creditCard = (CreditCardPaymentInfoModel) abstractOrder.getPaymentInfo();
			paymentMethod.type = getProperty(ChaseWepayConstants.WEPAY_PROPERTY_PAYMENT_METHODTYPE + "cc"); // WePay payment method type

			final PMCreditCardData ccData = new PMCreditCardData();
			ccData.autoCapture = Boolean.valueOf(autoCapture); // Auto Capture flag
			ccData.id = Long.valueOf(creditCard.getSubscriptionId()); // Card's subscription id
			paymentMethod.creditCard = ccData;
		}
		else
		{
			LOG.error("Unsupported payment info for # " + guid);
			throw new PaymentException(TransactionStatusDetails.INVALID_REQUEST.toString());
		}
		checkoutRequest.paymentMethod = paymentMethod;

		// Set fee details
		final FeeData feeData = new FeeData();
		feeData.feePayer = getProperty(ChaseWepayConstants.WEPAY_PROPERTY_ORDER_PAYMENT_FEEPAYER); // FeePayer is Merchant by default
		checkoutRequest.fee = feeData;

		// Generate & set payer rbits
		checkoutRequest.payerRbits = getCheckoutRbitsForType(abstractOrder, "payer_rbits");

		// Generate & set transaction rbits
		checkoutRequest.transactionRbits = getCheckoutRbitsForType(abstractOrder, "transaction_rbits");

		//setting callback URI
		checkoutRequest.callbackUri = callBackURI;
		return checkoutRequest;
	}

	/**
	 * Returns the checkout rbits for an rbit type.
	 *
	 * @param abstractOrder
	 *           - AbstractOrderModel
	 * @param rbitName
	 *           - String
	 * @return RbitData[]
	 */
	private RbitData[] getCheckoutRbitsForType(final AbstractOrderModel abstractOrder, final String rbitName)
	{
		LOG.info("Inside method - getCheckoutRbitsForType for # " + abstractOrder.getGuid());
		final String checkoutRbitTypeStr = checkoutRbitMap.get(rbitName);
		if (StringUtils.isNotBlank(checkoutRbitTypeStr))
		{
			final String rbitSource = getProperty(ChaseWepayConstants.WEPAY_PROPERTY_PAYMENT_CHECKOUTRBIT_SOURCE);
			final Long timeStamp = new Date().getTime() / 1000l;

			// Payer rbit types to be populated in payment request
			final List<String> payerRbitTypes = Arrays.asList(checkoutRbitTypeStr.split(","));

			if (CollectionUtils.isNotEmpty(payerRbitTypes))
			{
				final List<RbitData> payerRbits = new ArrayList<>();

				payerRbitTypes.stream().forEach(rbitType -> {
					final RbitData rbitData = generateCheckoutRbit(abstractOrder, rbitType, rbitSource, timeStamp);
					if (rbitData != null)
					{
						payerRbits.add(rbitData);
					}
				});

				return payerRbits.toArray(new RbitData[payerRbits.size()]);
			}

		}
		LOG.warn("No checkout rbits are created for # " + abstractOrder.getGuid());
		return null;
	}

	/**
	 * Returns checkout rbits.
	 *
	 * @param abstractOrder
	 *           - AbstractOrderModel
	 * @param rbitType
	 *           - String
	 * @param rbitSource
	 *           - String
	 * @param timeStamp
	 *           - Long
	 * @return RbitData
	 */
	private RbitData generateCheckoutRbit(final AbstractOrderModel abstractOrder, final String rbitType, final String rbitSource,
			final Long timeStamp)
	{
		final UserModel userModel = abstractOrder.getUser();

		// User's phone number details
		final Collection<UserPhoneNumberModel> userPhoneNumbers = userModel.getPhoneNumbers();
		PhoneNumberModel phoneNumber = null;
		if (CollectionUtils.isNotEmpty(userPhoneNumbers))
		{
			for (final UserPhoneNumberModel userPhoneNumber : userPhoneNumbers)
			{
				phoneNumber = userPhoneNumber.getPhoneNumber();
				break;
			}
		}

		// Get rbit type properties that are needed to be populated
		final List<String> rbitTypeProperties = rbitTypePropertiesMapping.get(rbitType);

		if (CollectionUtils.isNotEmpty(rbitTypeProperties))
		{
			final Map<String, Object> rbitProperties = new HashMap<>();

			// Check the rbitProperty and populate its relevant values
			for (final String rbitTypeProperty : rbitTypeProperties)
			{
				switch (rbitTypeProperty)
				{
					case "phone":
						if (phoneNumber != null)
						{
							rbitProperties.put(rbitTypeProperty, phoneNumber.getNumber());
						}
						break;

					case "email":
						final String emailAddress = userModel.getUid();
						if (StringUtils.isNotBlank(emailAddress))
						{
							rbitProperties.put(rbitTypeProperty, emailAddress);
						}
						break;

					case "address":
						final PaymentInfoModel paymentInfo = abstractOrder.getPaymentInfo();
						if (paymentInfo != null)
						{
							final AddressModel billingAddress = paymentInfo.getBillingAddress();
							final AddressData addressObject = getAddressRbitData(billingAddress);
							if (addressObject != null)
							{
								rbitProperties.put(rbitTypeProperty, addressObject);
							}
						}
						break;

					case "shipping_address":
						final AddressModel deliveryAddress = abstractOrder.getDeliveryAddress();
						if (deliveryAddress != null)
						{
							final AddressData addressObject = getAddressRbitData(deliveryAddress);
							if (addressObject != null)
							{
								rbitProperties.put(rbitTypeProperty, addressObject);
							}
						}
						break;

					case "itemized_receipt":
						final List<ItemReceiptData> itemReceipts = chaseWepayHelper.getTransactionItemReceiptDetails(abstractOrder);
						if (CollectionUtils.isNotEmpty(itemReceipts))
						{
							final ItemReceiptData[] itemReceiptArr = itemReceipts.toArray(new ItemReceiptData[itemReceipts.size()]);
							rbitProperties.put(rbitTypeProperty, itemReceiptArr);
						}
						break;

					default:
						break;
				}
			}
			if (MapUtils.isNotEmpty(rbitProperties))
			{
				return createRbitData(rbitType, rbitSource, timeStamp, rbitProperties);
			}
		}
		LOG.warn("No Checkout rbits were created for # " + abstractOrder.getGuid());
		return null;
	}

	/**
	 * Returns the address rbit data.
	 *
	 * @param addressModel
	 *           - AddressModel
	 * @return AddressData
	 */
	private AddressData getAddressRbitData(final AddressModel addressModel)
	{
		final AddressData addressData = new AddressData();
		addressData.setAddress1(addressModel.getLine1());
		addressData.setAddress2(addressModel.getLine2());
		addressData.setCity(addressModel.getTown());
		addressData.setPostal_code(addressModel.getPostalcode());
		final RegionModel region = addressModel.getRegion();
		if (region != null)
		{
			addressData.setRegion(region.getIsocodeShort());
		}
		final CountryModel country = addressModel.getCountry();
		if (country != null)
		{
			addressData.setCountry(country.getIsocode());
		}
		return addressData;
	}

	/**
	 * Creates the rbit data.
	 *
	 * @param rbitType
	 *           - String
	 * @param rbitSource
	 *           - String
	 * @param timeStamp
	 *           - Long
	 * @param rbitProperties
	 *           -Map<String, Object>
	 * @return RbitData
	 */
	private RbitData createRbitData(final String rbitType, final String rbitSource, final Long timeStamp,
			final Map<String, Object> rbitProperties)
	{
		final RbitData rbit = new RbitData();
		rbit.type = rbitType;
		rbit.source = rbitSource;
		rbit.receiveTime = timeStamp;
		rbit.properties = rbitProperties;
		return rbit;
	}

	@Override
	public String registerMerchantAndGetToken(final PointOfServiceModel merchant)
	{
		final UserData userData = new UserData();
		final AddressModel merchantAddress = merchant.getAddress();
		User userResponse = null;
		try
		{
			userData.email = merchantAddress.getEmail();
			userData.firstName = merchantAddress.getFirstname();
			userData.lastName = merchantAddress.getLastname();
			userData.originalIp = InetAddress.getLocalHost().getHostAddress();
			userData.originalDevice = configurationService.getConfiguration()
					.getString(ChaseWepayConstants.WEPAY_MERCHNT_REGISTRATION_DEVICE);
			userData.scope = configurationService.getConfiguration().getString(ChaseWepayConstants.WEPAY_MERCHNT_REGISTRATION_SCOPE);
			final Long tosAcceptanceTime = new Date().getTime() / 1000l;
			userData.tosAcceptanceTime = tosAcceptanceTime;
			if (LOG.isDebugEnabled())
			{
				LOG.debug("register merchant request: " + getRequestObjectInJson(userData));
			}
			userResponse = User.register(userData, new HeaderData());
			if (LOG.isDebugEnabled())
			{
				LOG.debug("register merchant response: " + getRequestObjectInJson(userResponse));
			}
			final String accessToken = userResponse.getAccessToken();
			if (StringUtils.isNotBlank(accessToken))
			{
				return accessToken;
			}
			else
			{
				throw new PaymentException("accessToken is not returned in response:" + getRequestObjectInJson(userResponse));
			}
		}
		catch (final UnknownHostException connectivityIssue)
		{
			waitForConfiguredTime(connectivityIssue);
			return registerMerchantAndGetToken(merchant);
		}
		catch (final JSONException | IOException e)
		{
			LOG.error("IOException|JSONException occurredwhile registering merchant:" + merchant.getName(), e);
			LOG.error("request: " + getRequestObjectInJson(userData) + " \n  response: " + getRequestObjectInJson(userResponse));
			throw new PaymentException(TransactionStatusDetails.COMMUNICATION_PROBLEM.toString(), e.getCause());
		}
		catch (final WePayException e)
		{
			LOG.error("WePayException occurred while registering merchant:" + merchant.getName(), e);
			LOG.error("request: " + getRequestObjectInJson(userData) + " \n  response: " + getRequestObjectInJson(userResponse));
			throw new PaymentException(processErrorDetails(e).toString(), e.getCause());
		}
	}

	/**
	 *
	 */
	private String getRequestObjectInJson(final Object userData)
	{
		if (userData != null)
		{
			try
			{
				return objectMapper.writeValueAsString(userData);
			}
			catch (final JsonProcessingException e)
			{
				LOG.error("couldn't print response due to: ", e);
			}
		}
		return StringUtils.EMPTY;
	}

	@Override
	public Long createMerchantAccountAndGetAccountId(final PointOfServiceModel merchant, final String merchantAccessToken)
	{
		final AccountData accountData = new AccountData();
		final AddressModel merchantAddress = merchant.getAddress();
		final String merchanId = merchant.getName().substring(3);
		Account accountResponse = null;
		try
		{
			accountData.country = merchantAddress.getCountry().getIsocode();
			final List<String> currencies = new ArrayList();
			for (final CurrencyModel currencyModel : merchant.getBaseStore().getCurrencies())
			{
				currencies.add(currencyModel.getIsocode());
			}
			accountData.currencies = currencies.toArray(new String[currencies.size()]);
			accountData.description = merchanId;
			accountData.name = merchant.getDisplayName();
			accountData.referenceId = merchanId;
			accountData.type = "business";
			final String rbitSource = configurationService.getConfiguration()
					.getString(ChaseWepayConstants.WEPAY_MERCHNT_REGISTRATION_RBIT_SOURCE);
			final List<String> rbitTypes = Arrays.asList(configurationService.getConfiguration()
					.getString(ChaseWepayConstants.WEPAY_MERCHNT_REGISTRATION_RBIT_TYPES).split(","));
			final RbitData[] rbits = generateMerchantRbits(merchant, rbitSource, rbitTypes, new Date().getTime() / 1000l);
			if (ArrayUtils.isNotEmpty(rbits))
			{
				accountData.rbits = rbits;
			}
			if (LOG.isDebugEnabled())
			{
				LOG.debug("create Merchant request: " + getRequestObjectInJson(accountData));
			}
			accountResponse = Account.create(accountData, merchantAccessToken);
			if (LOG.isDebugEnabled())
			{
				LOG.debug("create Merchant response: " + getRequestObjectInJson(accountResponse));
			}
			final Long accountId = accountResponse.getAccountId();
			if (accountId != null)
			{
				return accountId;
			}
			else
			{
				LOG.error("accountId is not returned from Wepay. \n request: " + getRequestObjectInJson(accountData)
						+ " \n  response: " + getRequestObjectInJson(accountData));
				throw new PaymentException("accountId is not returned from Wepay");
			}
		}
		catch (final UnknownHostException connectivityIssue)
		{
			waitForConfiguredTime(connectivityIssue);
			return createMerchantAccountAndGetAccountId(merchant, merchantAccessToken);
		}
		catch (final JSONException | IOException e)
		{
			LOG.error("IOException|JSONException occurred while creating merchant:" + merchant.getName(), e);
			LOG.error("request: " + getRequestObjectInJson(accountData) + " \n  response: " + getRequestObjectInJson(accountData));
			throw new PaymentException(TransactionStatusDetails.COMMUNICATION_PROBLEM.toString(), e.getCause());
		}
		catch (final WePayException e)
		{
			LOG.error("WePayException occurredwhile creating merchant:" + merchant.getName() + getErrorMessage(e), e);
			LOG.error("request: " + getRequestObjectInJson(accountData) + " \n  response: " + getRequestObjectInJson(accountData));
			throw new PaymentException(processErrorDetails(e).toString(), e.getCause());
		}
	}

	/**
	 *
	 */
	private void waitForConfiguredTime(final UnknownHostException connectivityIssue)
	{
		final long waitTime = configurationService.getConfiguration()
				.getLong(ChaseWepayConstants.WEPAY_CONNECTIVITY_RETRY_MILLISECONDS);
		LOG.error("Facing connectivity issues with wePay while onboarding merchant.", connectivityIssue);
		LOG.info("Waiting for " + waitTime + " before re-connecting...");
		try
		{
			TimeUnit.MILLISECONDS.sleep(waitTime);
		}
		catch (final InterruptedException e)
		{
			LOG.error("sleep time interrupted");
		}
	}

	@Override
	public void sendConfirmationEmailToMerchant(final String merchantAccessToken, final String merchantName)
	{
		try
		{
			User.sendConfirmation(merchantAccessToken);
		}
		catch (final JSONException | IOException e)
		{
			LOG.error("IOException|JSONException occurred while sending confirmation email to" + merchantName, e);
			throw new PaymentException(TransactionStatusDetails.COMMUNICATION_PROBLEM.toString(), e.getCause());
		}
		catch (final WePayException e)
		{
			LOG.error("WePayException occurredwhile creating merchant:" + merchantName + getErrorMessage(e), e);
			throw new PaymentException(processErrorDetails(e).toString(), e.getCause());
		}
	}

	/**
	 * Generates merchant rbits used in wepay account creation
	 */
	private RbitData[] generateMerchantRbits(final PointOfServiceModel merchant, final String rbitSource,
			final List<String> rbitTypes, final Long timeStamp)
	{
		final List<RbitData> rbits = new ArrayList();
		if (StringUtils.isNotEmpty(rbitSource) && CollectionUtils.isNotEmpty(rbitTypes))
		{
			for (final String rbitType : rbitTypes)
			{
				final RbitData rbit = generateMerchantRbit(merchant, rbitSource, rbitType, timeStamp);
				if (rbit != null)
				{
					rbits.add(rbit);
				}
			}
		}
		return rbits.toArray(new RbitData[rbits.size()]);
	}

	/**
	 * Generates merchant rbit for allowed rbit type
	 */
	private RbitData generateMerchantRbit(final PointOfServiceModel merchant, final String rbitSource, final String rbitType,
			final Long timeStamp)
	{
		final List<String> propertyKeys = rbitTypePropertiesMapping.get(rbitType);
		if (CollectionUtils.isNotEmpty(propertyKeys))
		{
			final Map<String, Object> rbitProperties = getMerchantRbitProperties(merchant, propertyKeys);
			if (MapUtils.isNotEmpty(rbitProperties))
			{
				return createRbitData(rbitType, rbitSource, timeStamp, rbitProperties);
			}
		}
		return null;
	}

	/**
	 * Sets the merchant rbit properties
	 */
	private Map<String, Object> getMerchantRbitProperties(final PointOfServiceModel merchant, final List<String> propertyKeys)
	{
		final Map<String, Object> properties = new HashMap();
		final AddressModel address = merchant.getAddress();
		for (final String property : propertyKeys)
		{
			switch (property)
			{
				case "business_name":
					properties.put(property, merchant.getDisplayName());
					break;
				case "business_description":
					properties.put(property, merchant.getName().substring(3));
					break;
				case "name":
					properties.put(property, address.getFirstname() + " " + address.getLastname());
					break;
				case "email":
					properties.put(property, address.getEmail());
					break;
				case "phone":
					final String phone = address.getPhone1();
					if (StringUtils.isNotBlank(phone))
					{
						properties.put(property, address.getPhone1());
					}
					break;
				case "phone_type":
					if (StringUtils.isNotBlank(address.getPhone1()))
					{
						properties.put(property, "mobile");
					}
					break;
				case "uri":
					final String uri = address.getUrl();
					if (StringUtils.isNotBlank(uri))
					{
						properties.put(property, address.getUrl());
					}
					break;
				default:
					break;
			}
		}
		return properties;
	}

	/**
	 * To generate error message from WePayException
	 */
	private String getErrorMessage(final WePayException e)
	{
		return " Error : " + e.getError() + " & ErrorCode : " + e.getErrorCode() + " & ErrorMessage : " + e.getErrorDescription();
	}

	/**
	 * To process error code returned from WePay
	 */
	private TransactionStatusDetails processErrorDetails(final WePayException e)
	{
		final Integer errorCode = e.getErrorCode();
		String transactionStatus = null;
		if (errorCode != null)
		{
			transactionStatus = getProperty(ChaseWepayConstants.WEPAY_ERRORCODE_PREFIX + errorCode);
		}
		return StringUtils.isNotBlank(transactionStatus) ? TransactionStatusDetails.valueOf(transactionStatus)
				: TransactionStatusDetails.CHASEWEPAY_ERROR;
	}

	/**
	 * This method will call Wepay API /checkout/capture capture the payment.
	 *
	 * @param checkoutId
	 *           - Long
	 * @return {@link PaymentData}
	 */
	@Override
	public PaymentData capturePayment(final Long checkoutId)
	{
		final OrderModel orderModel = chaseWepayHelper.getOrderForCheckoutId(checkoutId);
		final PointOfServiceModel mainDealer = orderModel.getMainDealer();
		validateParameterNotNull(mainDealer, "Dealer details for WePay - Payment capture are not found");
		final String guid = orderModel.getGuid();
		LOG.info("Entered method - capturePayment for # " + guid);
		final String wepayAccessToken = mainDealer.getWepayAccessToken();
		validateParameterNotNull(wepayAccessToken, "Dealer's WePay access token cannot be null");
		final PaymentData captureResult = new PaymentData();
		final Map<String, String> captureResponseParams = new HashMap<>();
		captureResult.setParameters(captureResponseParams);
		try
		{
			//capture payment
			final Checkout checkoutResponse = Checkout.capture(checkoutId, wepayAccessToken);
			final Long captureTxnRef = checkoutResponse.getCheckoutId();
			validateParameterNotNull(captureTxnRef, "Capture transaction reference cannot be null");
			captureResponseParams.put(ChaseWepayConstants.WEPAY_CAPTURE_CHECKOUT_ID, captureTxnRef.toString());
			final String captureState = checkoutResponse.getState();
			if (ChaseWepayConstants.WEPAY_PAYMENT_AUTHORIZED.equals(captureState))
			{
				captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS, TransactionStatus.REVIEW.toString());
				captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS_DETAILS,
						TransactionStatusDetails.REVIEW_NEEDED.toString());
			}
			else if (ChaseWepayConstants.WEPAY_PAYMENT_CAPTURED.equals(captureState)
					|| ChaseWepayConstants.WEPAY_PAYMENT_RELEASED.equals(captureState))
			{
				captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS, TransactionStatus.ACCEPTED.toString());
				captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS_DETAILS,
						TransactionStatusDetails.SUCCESFULL.toString());
			}
			else if (ChaseWepayConstants.WEPAY_PAYMENT_CANCELLED.equals(captureState))
			{
				captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS, TransactionStatus.REJECTED.toString());
				captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS_DETAILS,
						TransactionStatusDetails.PROCESSOR_DECLINE.toString());
			}
			else
			{
				LOG.error("Payment capture failed for # " + guid + " with CaptureTxnRef : " + captureTxnRef + " state : "
						+ captureState + " for CheckoutId : " + checkoutId);
				captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS, TransactionStatus.ERROR.toString());
				captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS_DETAILS,
						TransactionStatusDetails.CHASEWEPAY_ERROR.toString());
			}

			if (LOG.isDebugEnabled())
			{
				LOG.debug("Checkout Id for capture : " + WePayResource.gson.toJson(checkoutId) + " && Authorization response : "
						+ WePayResource.gson.toJson(checkoutResponse));
			}
			LOG.info("Exit method - capturePayment for # " + guid + " with CaptureTxnRef : " + captureTxnRef + " & state : "
					+ captureState);

		}
		catch (final SocketTimeoutException e)
		{
			LOG.error("SocketTimeoutException occured while capturing payment for cart # " + guid, e);
			captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS, TransactionStatus.ERROR.toString());
			captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS_DETAILS,
					TransactionStatusDetails.COMMUNICATION_PROBLEM.toString());
		}
		catch (JSONException | IOException e)
		{
			LOG.error("IOException|JSONException occurred while capturing payment for cart # " + guid, e);
			captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS, TransactionStatus.ERROR.toString());
			captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS_DETAILS,
					TransactionStatusDetails.CHASEWEPAY_ERROR.toString());
		}
		catch (final WePayException e)
		{
			LOG.error("WePayException occurred while capturing payment for cart # " + guid + " & Error : " + getErrorMessage(e), e);
			captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS, TransactionStatus.ERROR.toString());
			captureResponseParams.put(ChaseWepayConstants.TRANSACTION_STATUS_DETAILS, processErrorDetails(e).toString());
		}
		return captureResult;
	}

	public Checkout fetchCheckoutFromWepay(final Long checkoutId, final String wepayAccessToken)
	{
		Checkout checkout = new Checkout(checkoutId);
		try
		{
			checkout = Checkout.fetch(checkoutId, wepayAccessToken);
		}
		catch (JSONException | IOException e)
		{
			LOG.error("IOException|JSONException occurred while fetching checkout with Id#" + checkoutId);
			throw new PaymentException(TransactionStatusDetails.COMMUNICATION_PROBLEM.toString(), e.getCause());
		}
		catch (final WePayException e)
		{
			LOG.error("WePayException occurred while fetching checkout with Id#" + checkoutId);
			throw new PaymentException(processErrorDetails(e).toString(), e.getCause());
		}
		return checkout;
	}

	/**
	 * To read the property using Configuration service
	 */
	private String getProperty(final String property)
	{
		return configurationService.getConfiguration().getString(property);
	}

	/**
	 * @param modelService
	 *           - ModelService
	 */
	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @param flexibleSearchService
	 *           - FlexibleSearchService
	 */
	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}

	/**
	 * @param configurationService
	 *           - ConfigurationService
	 */
	@Required
	public void setConfigurationService(final ConfigurationService configurationService)
	{
		this.configurationService = configurationService;
	}

	/**
	 * @param enumerationService
	 *           - EnumerationService
	 */
	@Required
	public void setEnumerationService(final EnumerationService enumerationService)
	{
		this.enumerationService = enumerationService;
	}

	/**
	 * @param chaseWepayHelper
	 *           - ChaseWepayHelper
	 */
	@Required
	public void setChaseWepayHelper(final ChaseWepayHelper chaseWepayHelper)
	{
		this.chaseWepayHelper = chaseWepayHelper;
	}
}
