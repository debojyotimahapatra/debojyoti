/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.chase.wepay.payment.service;

import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.math.BigDecimal;

import com.wepay.model.Checkout;


/**
 * ChaseWepayService An interface that provides different payment services to integrate with Wepay
 *
 * @author skotni
 */
public interface ChaseWepayService
{
	/**
	 * This method will call wepay API /credit_card/authorize once the credit_card_id is received from FrontEnd i.e.
	 * isNewCreditCard is true.
	 *
	 * It will fetch credit card details using wepay API /credit_card i.e. isNewCreditCard is false.
	 *
	 * Response of this API contains all the credit card details.
	 *
	 * @param creditCardId
	 *           - String
	 * @param isNewCreditCard
	 *           - boolean
	 * @return CardInfo
	 */
	CardInfo getCreditCardDetails(String creditCardId, boolean isNewCreditCard);

	/**
	 * Wepay API /user/register
	 *
	 * @param merchant
	 * @return merchant access_token
	 */
	String registerMerchantAndGetToken(PointOfServiceModel merchant);

	/**
	 * Wepay API /account/create
	 *
	 * @param merchant
	 * @param merchantAccessToken
	 * @return account_id
	 */
	Long createMerchantAccountAndGetAccountId(PointOfServiceModel merchant, String merchantAccessToken);

	/**
	 * Wepay API /account/get_update_url
	 *
	 * @param merchantName
	 * @param merchantAccessToken
	 */
	void sendConfirmationEmailToMerchant(String merchantAccessToken, String merchantName);

	/**
	 * This method will call Wepay API /credit_card/delete to delete a credit card added by customer.
	 *
	 * @param creditCardId
	 *           - Long
	 * @return boolean
	 */
	boolean deleteCreditCard(Long creditCardId);

	/**
	 * This method will call Wepay API /checkout/create to do final payment authorization.
	 *
	 * @param abstractOrder
	 *           - AbstractOrderModel
	 * @param totalAmount
	 *           the - BigDecimal
	 * @param currencyCode
	 *           - String
	 * @return {@link PaymentData}
	 */
	PaymentData authorizePayment(AbstractOrderModel abstractOrder, BigDecimal totalAmount, String currencyCode,
			boolean autoCapture);

	/**
	 * This method will call Wepay API /checkout/capture capture the payment.
	 *
	 * @param checkoutId
	 *           - Long
	 * @return {@link PaymentData}
	 */
	PaymentData capturePayment(Long checkoutId);

	/**
	 * This method will call the Wepay API /checkout - to fetch the checkout object for the given Id
	 *
	 * @param checkoutId
	 *           - Long
	 * @param wepayAccessToken
	 *           - String
	 */
	Checkout fetchCheckoutFromWepay(Long checkoutId, String wepayAccessToken);
}
