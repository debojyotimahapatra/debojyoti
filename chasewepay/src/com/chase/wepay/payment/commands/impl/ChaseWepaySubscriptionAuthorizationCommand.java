/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.chase.wepay.payment.commands.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.PaymentInfoModel;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.commands.SubscriptionAuthorizationCommand;
import de.hybris.platform.payment.commands.request.SubscriptionAuthorizationRequest;
import de.hybris.platform.payment.commands.result.AuthorizationResult;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.chase.wepay.payment.constants.ChaseWepayConstants;
import com.chase.wepay.payment.exception.PaymentException;
import com.chase.wepay.payment.service.ChaseWepayService;


/**
 * Subscription authorization command to do final payment authorization when customer places an order.
 */
public class ChaseWepaySubscriptionAuthorizationCommand implements SubscriptionAuthorizationCommand
{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ChaseWepaySubscriptionAuthorizationCommand.class.getSimpleName());

	/** The chase wepay service. */
	private ChaseWepayService chaseWepayService;

	/** The cart service. */
	private CartService cartService;

	@Override
	public AuthorizationResult perform(final SubscriptionAuthorizationRequest request)
	{
		final String creditCardId = request.getSubscriptionID();
		validateParameterNotNull(creditCardId, "SubscriptionId cannot be null");
		final BigDecimal totalAmount = request.getTotalAmount();
		validateParameterNotNull(creditCardId, "Authorization amount cannot be null");
		final CartModel cartModel = cartService.getSessionCart();
		validateParameterNotNull(cartModel, "Session cart cannot be null");
		final PaymentInfoModel paymentInfoModel = cartModel.getPaymentInfo();
		validateParameterNotNull(paymentInfoModel, "Cart's paymentInfo cannot be null");
		final Currency currency = request.getCurrency();
		validateParameterNotNull(currency, "Cart doesn't have currency");

		final String cartId = cartModel.getCode();
		final String paymentDetailsId = paymentInfoModel.getPk().toString();

		final PaymentData paymentAuthResponse = chaseWepayService.authorizePayment(cartModel,
				totalAmount, currency.getCurrencyCode(), false);
		validateParameterNotNull(paymentAuthResponse, "Payment authorization response cannot be null");
		final Map<String, String> responseParams = paymentAuthResponse.getParameters();
		validateParameterNotNull(responseParams, "Payment authorization response params cannot be null");

		final String transactionStatus = responseParams.getOrDefault(ChaseWepayConstants.TRANSACTION_STATUS,
				TransactionStatus.ERROR.toString());
		final String transactionStatusDetails = responseParams.getOrDefault(ChaseWepayConstants.TRANSACTION_STATUS_DETAILS,
				TransactionStatusDetails.CHASEWEPAY_ERROR.toString());
		final String checkoutId = responseParams.get(ChaseWepayConstants.WEPAY_AUTHORIZATION_CHECKOUT_ID);
		final String uniqueId = responseParams.get(ChaseWepayConstants.WEPAY_AUTHORIZATION_UNIQUE_ID);


		final AuthorizationResult result = new AuthorizationResult();
		result.setRequestId(uniqueId);
		result.setRequestToken(checkoutId);
		result.setTotalAmount(totalAmount);
		result.setCurrency(request.getCurrency());
		result.setPaymentProvider(request.getPaymentProvider());
		result.setTransactionStatus(TransactionStatus.valueOf(transactionStatus));
		if (TransactionStatus.ACCEPTED.toString().equals(transactionStatus))
		{
			LOG.info("Payment authorization is succeeded for Cart #" + cartId + " with Payment info #" + paymentDetailsId);
			result.setTransactionStatusDetails(TransactionStatusDetails.valueOf(transactionStatusDetails));
			return result;
		}
		else
		{
			LOG.error("Payment authorization failed for Cart #" + cartId + " with Payment info #" + paymentDetailsId
					+ " & UniqueTxnId : " + uniqueId
					+ " & Transaction Status Details : " + transactionStatusDetails + " & Amount : " + totalAmount);
			throw new PaymentException(transactionStatusDetails);
		}
	}

	/**
	 * @param cartService
	 *           - CartService
	 */
	@Required
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}

	/**
	 * @param chaseWepayService
	 *           - ChaseWepayService
	 */
	@Required
	public void setChaseWepayService(final ChaseWepayService chaseWepayService)
	{
		this.chaseWepayService = chaseWepayService;
	}

}
