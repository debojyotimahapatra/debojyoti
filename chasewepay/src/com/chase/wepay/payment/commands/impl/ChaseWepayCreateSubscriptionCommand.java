/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.chase.wepay.payment.commands.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.payment.commands.CreateSubscriptionCommand;
import de.hybris.platform.payment.commands.request.CreateSubscriptionRequest;
import de.hybris.platform.payment.commands.result.SubscriptionResult;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import org.apache.commons.lang3.StringUtils;


/**
 * This command will be executed when customer adds new payment details during checkout.
 *
 * @author skotni
 */
public class ChaseWepayCreateSubscriptionCommand implements CreateSubscriptionCommand
{

	/**
	 * Returns the subscription result
	 *
	 * @param request
	 *           - {@link CreateSubscriptionRequest}
	 * @return {@link SubscriptionResult}
	 */
	@Override
	public SubscriptionResult perform(final CreateSubscriptionRequest request)
	{
		final SubscriptionResult result = new SubscriptionResult();
		validateParameterNotNull(request, "CreateSubscriptionRequest cannot be null");
		final CardInfo cardInfo = request.getCard();
		if (cardInfo != null && StringUtils.isNotEmpty(cardInfo.getIssueNumber()))
		{
			result.setSubscriptionID(cardInfo.getIssueNumber());
			result.setTransactionStatus(TransactionStatus.ACCEPTED);
			result.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL);

			// Received issue number is of Long type but it is of Integer in Hybris.
			// Set issueNumber to null in CardInfo to avoid number format exception thrown from OOTB.
			cardInfo.setIssueNumber(null);
		}
		else
		{
			result.setTransactionStatus(TransactionStatus.REJECTED);
			result.setTransactionStatusDetails(TransactionStatusDetails.INVALID_REQUEST);
		}
		return result;
	}

}
