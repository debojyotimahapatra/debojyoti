/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.chase.wepay.payment.commands.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.payment.commands.DeleteSubscriptionCommand;
import de.hybris.platform.payment.commands.request.DeleteSubscriptionRequest;
import de.hybris.platform.payment.commands.result.SubscriptionResult;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.chase.wepay.payment.helper.ChaseWepayHelper;
import com.chase.wepay.payment.service.ChaseWepayService;


/**
 * ChaseWepayDeleteSubscriptionCommand
 *
 * @author skotni
 */
public class ChaseWepayDeleteSubscriptionCommand implements DeleteSubscriptionCommand
{
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ChaseWepayDeleteSubscriptionCommand.class);

	/** The chase wepay service. */
	private ChaseWepayService chaseWepayService;

	/** The chase wepay helper. */
	private ChaseWepayHelper chaseWepayHelper;

	/**
	 * To delete credit card from WePay
	 *
	 * @param deleteSubscriptionRequest
	 *           {@link DeleteSubscriptionRequest}
	 * @return {@link SubscriptionResult}
	 */
	@Override
	public SubscriptionResult perform(final DeleteSubscriptionRequest deleteSubscriptionRequest)
	{
		final String subscriptionId = deleteSubscriptionRequest.getSubscriptionID();
		validateParameterNotNull(subscriptionId, "SubscriptionId cannot be null");

		final List<OrderModel> capturePendingOrders = chaseWepayHelper.getPendingOrdersForCapture(subscriptionId);

		final SubscriptionResult result = new SubscriptionResult();
		result.setTransactionStatus(TransactionStatus.ERROR);
		if (CollectionUtils.isEmpty(capturePendingOrders))
		{
			// Delete the credit card in WePay only if theare no pending orders for capture payment
			final boolean isCardDeleted = chaseWepayService.deleteCreditCard(Long.valueOf(subscriptionId));
			result.setSubscriptionID(subscriptionId);

			if (isCardDeleted)
			{
				result.setTransactionStatus(TransactionStatus.ACCEPTED);
				result.setTransactionStatusDetails(TransactionStatusDetails.SUCCESFULL);
			}
			else
			{
				result.setTransactionStatusDetails(TransactionStatusDetails.CHASEWEPAY_ERROR);
			}
		}
		else
		{
			result.setTransactionStatusDetails(TransactionStatusDetails.PENDING_ORDERS_FOR_CAPTURE);
			LOG.error(
					"One or more orders are pending for payment capture. Cannot delete CreditCard # " + subscriptionId + " in WePay");
		}

		return result;
	}

	/**
	 * @param chaseWepayService
	 *           - ChaseWepayService
	 */
	@Required
	public void setChaseWepayService(final ChaseWepayService chaseWepayService)
	{
		this.chaseWepayService = chaseWepayService;
	}

	/**
	 * @param chaseWepayHelper
	 *           - ChaseWepayHelper
	 */
	@Required
	public void setChaseWepayHelper(final ChaseWepayHelper chaseWepayHelper)
	{
		this.chaseWepayHelper = chaseWepayHelper;
	}

}
