/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.chase.wepay.payment.commands.impl;

import de.hybris.platform.acceleratorservices.payment.data.PaymentData;
import de.hybris.platform.payment.commands.CaptureCommand;
import de.hybris.platform.payment.commands.request.CaptureRequest;
import de.hybris.platform.payment.commands.result.CaptureResult;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.dto.TransactionStatusDetails;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.Assert;

import com.chase.wepay.payment.constants.ChaseWepayConstants;
import com.chase.wepay.payment.service.ChaseWepayService;


/**
 * @author smalayanur
 */
public class ChaseWepayCaptureCommand implements CaptureCommand
{
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ChaseWepayCaptureCommand.class.getSimpleName());

	/** The wepay service. */
	private ChaseWepayService chaseWepayService;

	@Override
	public CaptureResult perform(final CaptureRequest captureRequest)
	{
		final String authTxnRef = captureRequest.getRequestToken();
		final Long checkoutId = Long.valueOf(authTxnRef);
		final String txnCode = captureRequest.getMerchantTransactionCode();
		//capture payment
		LOG.debug("executing captureCommand for Wepay for checkout ID:" + checkoutId);
		final PaymentData paymentData = getChaseWepayService().capturePayment(checkoutId);

		final Map<String, String> responseParameters = paymentData.getParameters();
		Assert.notNull(responseParameters, "Capture response parameters cannot be null");

		final String transactionStatus = responseParameters.get(ChaseWepayConstants.TRANSACTION_STATUS);
		final String transactionStatusDetails = responseParameters.get(ChaseWepayConstants.TRANSACTION_STATUS_DETAILS);
		final String captureTransactionRefNum = responseParameters.get(ChaseWepayConstants.WEPAY_CAPTURE_CHECKOUT_ID);

		final CaptureResult captureResult = new CaptureResult();
		captureResult.setRequestId(authTxnRef);
		captureResult.setRequestToken(captureTransactionRefNum);
		captureResult.setTransactionStatus(TransactionStatus.valueOf(transactionStatus));
		captureResult.setTransactionStatusDetails(TransactionStatusDetails.valueOf(transactionStatusDetails));

		if (TransactionStatus.ACCEPTED.equals(transactionStatus))
		{
			LOG.info("Capture payment is successful for txn # " + txnCode + " with AuthTxnRef : " + authTxnRef
					+ " & CaptureTxnRef : " + captureTransactionRefNum);
		}
		if (TransactionStatus.REVIEW.equals(transactionStatus))
		{
			LOG.warn("Capture payment is in review for txn # " + txnCode + " with AuthTxnRef : " + authTxnRef + " & CaptureTxnRef : "
					+ captureTransactionRefNum);
		}
		else if (TransactionStatus.ERROR.equals(transactionStatus)
				&& TransactionStatusDetails.COMMUNICATION_PROBLEM.equals(transactionStatusDetails))
		{
			LOG.error("Connectivity error while doing capture payment for txn # " + txnCode + " with AuthTxnRef : " + authTxnRef);
		}
		else
		{
			LOG.error("Unable to complete capture payment for txn #" + txnCode + " & Transaction Status : " + transactionStatus
					+ " & Transaction Status Details : " + transactionStatusDetails + " & Additional Details : " + responseParameters);
		}

		return captureResult;
	}

	/**
	 * @return the chaseWepayService
	 */
	protected ChaseWepayService getChaseWepayService()
	{
		return chaseWepayService;
	}

	/**
	 * @param chaseWepayService
	 *           the chaseWepayService to set
	 */
	@Required
	public void setChaseWepayService(final ChaseWepayService chaseWepayService)
	{
		this.chaseWepayService = chaseWepayService;
	}

}
