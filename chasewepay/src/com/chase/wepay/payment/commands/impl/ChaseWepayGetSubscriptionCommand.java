/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2018 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.chase.wepay.payment.commands.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNull;

import de.hybris.platform.payment.commands.GetSubscriptionDataCommand;
import de.hybris.platform.payment.commands.request.SubscriptionDataRequest;
import de.hybris.platform.payment.commands.result.SubscriptionDataResult;
import de.hybris.platform.payment.dto.CardInfo;

import org.springframework.beans.factory.annotation.Required;

import com.chase.wepay.payment.service.ChaseWepayService;

/**
 * This command has the implementation to interact with Wepay for authorizing
 * the credit card details entered by customer and returns the card details.
 *
 * @author skotni
 */
public class ChaseWepayGetSubscriptionCommand implements GetSubscriptionDataCommand
{

	/** The chase wepay service. */
	private ChaseWepayService chaseWepayService;

	/**
	 * Returns the subscription result with authorized credit card details
	 *
	 * @param request
	 *           - {@link SubscriptionDataRequest}
	 * @return {@link SubscriptionDataResult}
	 */
	@Override
	public SubscriptionDataResult perform(final SubscriptionDataRequest request)
	{
		validateParameterNotNull(request, "SubscriptionDataRequest cannot be null");
		final String wepayCreditCardId = request.getSubscriptionID();

		final CardInfo cardInfo = chaseWepayService.getCreditCardDetails(wepayCreditCardId, true);
		final SubscriptionDataResult result = new SubscriptionDataResult();
		result.setCard(cardInfo);
		return result;
	}

	/**
	 * @param chaseWepayService
	 *           - ChaseWepayService
	 */
	@Required
	public void setChaseWepayService(final ChaseWepayService chaseWepayService)
	{
		this.chaseWepayService = chaseWepayService;
	}

}
